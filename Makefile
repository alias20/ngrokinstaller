DEBUG=0
FINALPACKAGE=1
GO_EASY_ON_ME=1

THEOS_PACKAGE_SCHEME = rootless

THEOS_USE_NEW_ABI=1
TARGET = iphone:14.5:14.5
ARCHS = arm64 arm64e

THEOS_DEVICE_IP = 127.0.0.1 -p 2222

include $(THEOS)/makefiles/common.mk

TOOL_NAME = ngrokinstaller

ngrokinstaller_FILES = main.mm
ngrokinstaller_CFLAGS = -fobjc-arc
ngrokinstaller_CODESIGN_FLAGS = -Sentitlements.plist
ngrokinstaller_INSTALL_PATH = /usr/local/bin

include $(THEOS_MAKE_PATH)/tool.mk
